package br.edu.ifsp.bra.supermarketback.repositories;

import br.edu.ifsp.bra.supermarketback.domain.models.CashRegister.CashRegister;
import br.edu.ifsp.bra.supermarketback.domain.models.CashRegister.CashRegisterState;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.Date;

@RepositoryRestResource
@CrossOrigin(origins = "http://localhost:4200")
public interface CashRegisterRepository extends PagingAndSortingRepository<CashRegister, String> {
    Page<CashRegister> findAllByState(CashRegisterState state, Pageable pageable);
    Page<CashRegister> findAllByDaily(Date date, Pageable pageable);
}
