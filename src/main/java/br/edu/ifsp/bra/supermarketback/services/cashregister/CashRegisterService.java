package br.edu.ifsp.bra.supermarketback.services.cashregister;

import br.edu.ifsp.bra.supermarketback.domain.models.CashRegister.CashRegister;
import br.edu.ifsp.bra.supermarketback.domain.models.CashRegister.CashRegisterState;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.Date;
import java.util.Optional;

public interface CashRegisterService {
    Optional<CashRegister> findById(String id);
    Page<CashRegister> findByDaily(Date date, PageRequest pageRequest);
    Page<CashRegister> findByState(CashRegisterState state, PageRequest pageRequest);
    Page<CashRegister> findAll(PageRequest pageRequest);
}
