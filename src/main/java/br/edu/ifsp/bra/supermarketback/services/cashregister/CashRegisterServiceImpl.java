package br.edu.ifsp.bra.supermarketback.services.cashregister;

import br.edu.ifsp.bra.supermarketback.domain.models.CashRegister.CashRegister;
import br.edu.ifsp.bra.supermarketback.domain.models.CashRegister.CashRegisterState;
import br.edu.ifsp.bra.supermarketback.repositories.CashRegisterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;

@Service
public class CashRegisterServiceImpl implements CashRegisterService {

    final private CashRegisterRepository repository;

    @Autowired
    public CashRegisterServiceImpl(CashRegisterRepository repository) {
        this.repository = repository;
    }

    @Override
    public Optional<CashRegister> findById(String id) {
        return this.repository.findById(id);
    }

    @Override
    public Page<CashRegister> findByDaily(Date date, PageRequest pageRequest) {
        return this.repository.findAllByDaily(date, pageRequest);
    }

    @Override
    public Page<CashRegister> findByState(CashRegisterState state, PageRequest pageRequest) {
        return this.repository.findAllByState(state, pageRequest);
    }

    @Override
    public Page<CashRegister> findAll(PageRequest pageRequest) {
        return this.repository.findAll(pageRequest);
    }
}
