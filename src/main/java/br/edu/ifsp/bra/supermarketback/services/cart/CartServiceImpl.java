package br.edu.ifsp.bra.supermarketback.services.cart;

import br.edu.ifsp.bra.supermarketback.domain.models.Cart.Cart;
import br.edu.ifsp.bra.supermarketback.domain.models.Cart.CartItem;
import br.edu.ifsp.bra.supermarketback.repositories.Cart.CartRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CartServiceImpl implements CartService {

    private final CartRepository repository;

    @Autowired
    public CartServiceImpl(CartRepository repository) {
        this.repository = repository;
    }

    @Override
    public Cart getCartById(String id) {
        return this.repository.getCartById(id);
    }

    @Override
    public Cart addItemToCart(String id, CartItem item) {
        return this.repository.addToCart(id, item);
    }

    @Override
    public void removeItemToCart(String id, CartItem item) {
        this.repository.removeToCart(id, item);

    }
}
