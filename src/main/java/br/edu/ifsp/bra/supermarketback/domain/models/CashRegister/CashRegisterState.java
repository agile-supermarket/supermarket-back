package br.edu.ifsp.bra.supermarketback.domain.models.CashRegister;

public enum CashRegisterState {
    OPEN,
    CLOSED,
}
