package br.edu.ifsp.bra.supermarketback.controllers;

import br.edu.ifsp.bra.supermarketback.domain.models.Product.Product;
import br.edu.ifsp.bra.supermarketback.domain.models.Product.ProductCategory;
import br.edu.ifsp.bra.supermarketback.messages.ProductProducer;
import br.edu.ifsp.bra.supermarketback.services.product.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/product")
@CrossOrigin(origins = "http://localhost:4200")
public class ProductController {

    private final static Logger LOGGER = LoggerFactory.getLogger(ProductController.class);
    private final ProductService service;
    private final ProductProducer productProducer;

    @Autowired
    public ProductController(ProductService service, ProductProducer productProducer) {
        this.service = service;
        this.productProducer = productProducer;
    }

    @GetMapping()
    public ResponseEntity listProducts(@RequestParam(value = "name", required = false) String name,
                                       @RequestParam(value = "id", required = false) String id,
                                       @RequestParam(value = "barcode", required = false) String barcode,
                                       @RequestParam(value = "category", required = false) ProductCategory  category,
                                       @RequestParam(value = "offset", required = false, defaultValue = "0") int page,
                                       @RequestParam(value = "limit", required = false, defaultValue = "5") int size,
                                       Sort sort) {

        LOGGER.info("findAll: id: {} | name : {} | barcode {} | category: {} | offset: {} | limit: {} | sort: {}", id, name, barcode, category, page, size, sort);

        PageRequest pageRequest = PageRequest.of(page, size, sort);

        if (id != null) {
            return new ResponseEntity(this.service.getById(id), HttpStatus.OK);
        }

        if (name != null) {
            return new ResponseEntity(this.service.getByName(name, pageRequest), HttpStatus.OK);
        }

        if (name != null && category != null && !category.name().isEmpty()) {
            return new ResponseEntity(this.service.getByNameAndCategory(name, category, pageRequest), HttpStatus.OK);
        }

        if (category != null && !category.name().isEmpty()) {
            return new ResponseEntity(this.service.getByCategory(category, pageRequest), HttpStatus.OK);
        }

        if (barcode != null && !barcode.isEmpty()) {
            return new ResponseEntity(this.service.getByBarcode(barcode), HttpStatus.OK);
        }


        return new ResponseEntity<>(this.service.listAll(pageRequest), HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<Product> createProduct(@RequestBody Product product) {
        productProducer.sendProductMessage(product);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Product> updateProduct(@RequestBody Product product, @PathVariable String id) {

        var dbProduct = service.getById(id).orElse(product);

        dbProduct.setName(product.getName() != null && !product.getName().isEmpty()
                            ? product.getName() : dbProduct.getName());

        dbProduct.setPrice(product.getPrice() != null
                            ? product.getPrice() : dbProduct.getPrice());

        service.save(dbProduct);

        return new ResponseEntity<>(dbProduct, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity deleteProduct(@PathVariable String id) {
        service.deleteById(id);
        return new ResponseEntity(HttpStatus.OK);
    }
}
