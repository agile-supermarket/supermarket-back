package br.edu.ifsp.bra.supermarketback.controllers;

import br.edu.ifsp.bra.supermarketback.domain.entities.Stock;
import br.edu.ifsp.bra.supermarketback.domain.models.Product.Product;
import br.edu.ifsp.bra.supermarketback.services.Stock.StockService;
import br.edu.ifsp.bra.supermarketback.services.product.ProductService;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;


@RestController
@RequestMapping("/api/stock")
@CrossOrigin(origins = "http://localhost:4200")
public class StockController {

    private static final Logger LOGGER = (Logger) LoggerFactory.getLogger(StockController.class);

    private final StockService stockService;

    @Autowired
    public StockController(StockService stockService) {
        this.stockService = stockService;
    }


    @GetMapping()
    public ResponseEntity listStock(@RequestParam(value = "id", required = false) String id,
                                    @RequestParam(value = "product", required = false)Product product,
                                    @RequestParam(value = "quantity", required = false)String quantity,
                                    @RequestParam(value = "data", required = false)Date data,
                                    @RequestParam(value = "offset", required = false, defaultValue = "0") int page,
                                    @RequestParam(value = "limit", required = false, defaultValue = "5") int size,
                                    Sort sort){

        LOGGER.info("findAll: id: {} | product: {}| quantity: {} | data: {}| offset: {} | limit: {}|sort: {}", id ,  page, size, sort);

        PageRequest pageRequest = PageRequest.of(page,size,sort);

        if(id!=null){
            return new ResponseEntity(this.stockService.getById(id), HttpStatus.OK);
        }

        return new ResponseEntity<>(this.stockService.listAll(pageRequest), HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<Stock> addToStock(@RequestBody Stock stock){

        stockService.save(stock);

        return new ResponseEntity<>(stock, HttpStatus.OK);

    }


}
