package br.edu.ifsp.bra.supermarketback.domain.models.Cart;

import br.edu.ifsp.bra.supermarketback.domain.models.Product.Product;
import com.mongodb.lang.NonNull;
import lombok.*;
import org.springframework.data.redis.core.RedisHash;

import javax.persistence.Id;
import java.io.Serializable;

@RedisHash("CartItem")
@Getter @Setter @NoArgsConstructor @EqualsAndHashCode
public class CartItem implements Serializable {
    @Id
    private String id;
    @NonNull
    private Product product;
    @NonNull
    private float quantity;
    private float price;
}
