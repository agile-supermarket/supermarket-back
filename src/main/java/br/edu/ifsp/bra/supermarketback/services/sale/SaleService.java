package br.edu.ifsp.bra.supermarketback.services.sale;

import br.edu.ifsp.bra.supermarketback.domain.entities.Employee;
import br.edu.ifsp.bra.supermarketback.domain.entities.Sale;
import br.edu.ifsp.bra.supermarketback.domain.models.Cart.Cart;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.Optional;

public interface SaleService {
    Sale save(Sale sale);
    Optional<Sale> findById(String id);
    Page<Sale> getAll(PageRequest pageRequest);
    Page<Sale> getByEmployee(Employee employee, PageRequest pageRequest);
    Page<Sale> getByCart(Cart cart, PageRequest pageRequest);
}
