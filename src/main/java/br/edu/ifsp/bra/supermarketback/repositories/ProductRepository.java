package br.edu.ifsp.bra.supermarketback.repositories;

import br.edu.ifsp.bra.supermarketback.domain.models.Product.Product;
import br.edu.ifsp.bra.supermarketback.domain.models.Product.ProductCategory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.Optional;

@RepositoryRestResource
@CrossOrigin(origins = "http://localhost:4200")
public interface ProductRepository extends PagingAndSortingRepository<Product, String> {
    Optional<Product> findById(String  id);
    Page<Product> findByName(String  name, Pageable pageable);
    Page<Product> findByNameAndCategory(String  name, ProductCategory category, Pageable pageable);
    Page<Product> findByCategory(ProductCategory category, Pageable pageable);
    Optional<Product> findByBarcode(String barcode);
}
