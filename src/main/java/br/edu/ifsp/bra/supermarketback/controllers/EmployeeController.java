package br.edu.ifsp.bra.supermarketback.controllers;

import br.edu.ifsp.bra.supermarketback.domain.entities.Employee;
import br.edu.ifsp.bra.supermarketback.services.employee.EmployeeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/employee")
@CrossOrigin(origins = "http://localhost:4200")
public class EmployeeController {

    private final EmployeeService service;
    private final static Logger LOGGER = LoggerFactory.getLogger(EmployeeController.class);


    @Autowired
    public EmployeeController(EmployeeService service) {
        this.service = service;
    }

    @GetMapping("/")
    public Collection<Employee> listEmployees() {
        return service.listAll()
                .stream()
                .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public Employee getEmployee(@PathVariable String id) {
        return service.getById(id).orElse(null);
    }

    @PostMapping("/")
    public ResponseEntity<Employee> insert(@RequestBody Employee employee) {
        service.insert(employee);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Employee> update(@RequestBody Employee employee, @PathVariable String id) {
        Employee dbEmployee = this.service.getById(id).orElse(employee);
        dbEmployee.setName(employee.getName() != null && !employee.getName().isEmpty() ? employee.getName() : dbEmployee.getName());
        dbEmployee.setPassword(employee.getPassword() != null && !employee.getPassword().isEmpty() ? employee.getPassword() : dbEmployee.getPassword());
        dbEmployee.setName(employee.getName() != null && !employee.getName().isEmpty() ? employee.getName() : dbEmployee.getName());
        service.update(employee);
        return new ResponseEntity<>(dbEmployee, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity delete(@PathVariable String id) {
        this.service.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }




}
