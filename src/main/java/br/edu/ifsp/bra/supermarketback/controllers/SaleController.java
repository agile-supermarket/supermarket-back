package br.edu.ifsp.bra.supermarketback.controllers;

import br.edu.ifsp.bra.supermarketback.domain.entities.Employee;
import br.edu.ifsp.bra.supermarketback.domain.entities.Sale;
import br.edu.ifsp.bra.supermarketback.domain.models.Cart.Cart;
import br.edu.ifsp.bra.supermarketback.services.sale.SaleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/sale")
@CrossOrigin(origins = "http://localhost:4200")
public class SaleController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CartController.class);

    private final SaleService service;

    @Autowired
    public SaleController(SaleService service) {
        this.service = service;
    }

    @GetMapping()
    public ResponseEntity getSales(@RequestParam(value = "id", required = false) String id,
                                   @RequestParam(value = "employee", required = false) Employee employee,
                                   @RequestParam(value = "cart", required = false) Cart cart,
                                   @RequestParam(value = "offset", required = false, defaultValue = "0") int page,
                                   @RequestParam(value = "limit", required = false, defaultValue = "5") int size,
                                   Sort sort
    ) {

        LOGGER.info("findAll: id: {} | employee : {} | cart {} | offset: {} | limit: {} | sort: {}", id, employee, cart, page, size, sort);
        PageRequest pageRequest = PageRequest.of(page, size, sort);


        if (id != null) {
            return new ResponseEntity(this.service.findById(id), HttpStatus.OK);
        }

        if (employee != null) {
            return new ResponseEntity(this.service.getByEmployee(employee, pageRequest), HttpStatus.OK);
        }

        if (cart != null) {
            return new ResponseEntity(this.service.getByCart(cart, pageRequest), HttpStatus.OK);
        }

        return new ResponseEntity(this.service.getAll(pageRequest), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Sale> createSale(@RequestBody Sale sale) {
       var dbSale = this.service.save(sale);
       return new ResponseEntity<>(dbSale, HttpStatus.OK);
    }
}
