package br.edu.ifsp.bra.supermarketback.repositories.Cart;

import br.edu.ifsp.bra.supermarketback.domain.models.Cart.Cart;
import br.edu.ifsp.bra.supermarketback.domain.models.Cart.CartItem;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

@RepositoryRestResource
@CrossOrigin(origins = "http://localhost:4200")
public interface CartRepository {
    Cart getCartById(String id);

    Cart addToCart(String id, CartItem item);

    void removeToCart(String id, CartItem item);
}
