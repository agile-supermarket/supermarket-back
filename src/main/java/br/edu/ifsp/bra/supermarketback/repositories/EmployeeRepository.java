package br.edu.ifsp.bra.supermarketback.repositories;

import br.edu.ifsp.bra.supermarketback.domain.entities.Employee;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.Set;

@RepositoryRestResource
@CrossOrigin(origins = "http://localhost:4200")
public interface EmployeeRepository extends MongoRepository<Employee, String> {
    Employee findByName(String name);
}
