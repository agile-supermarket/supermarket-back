package br.edu.ifsp.bra.supermarketback.security;

import br.edu.ifsp.bra.supermarketback.domain.entities.Employee;
import br.edu.ifsp.bra.supermarketback.services.employee.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class SecurityUserDetail implements UserDetailsService {

    private final EmployeeService employeeService;

    @Autowired
    public SecurityUserDetail(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        BCryptPasswordEncoder encoder = passwordEncoder();

        Employee employee = this.employeeService.getByName(username)
                .orElseThrow(() -> new UsernameNotFoundException("User not found!!!"));

        List<GrantedAuthority> admin = AuthorityUtils
                .createAuthorityList("ADMIN","USER");

        List<GrantedAuthority> user = AuthorityUtils
                .createAuthorityList("USER");


        return new org.springframework.security.core.userdetails.User(
                employee.getName(),
                encoder.encode(employee.getPassword()),
                employee.isAdmin() ? admin : user);

    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
