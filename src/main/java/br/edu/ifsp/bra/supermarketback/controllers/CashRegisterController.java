package br.edu.ifsp.bra.supermarketback.controllers;

import br.edu.ifsp.bra.supermarketback.domain.models.CashRegister.CashRegister;
import br.edu.ifsp.bra.supermarketback.domain.models.CashRegister.CashRegisterState;
import br.edu.ifsp.bra.supermarketback.services.cashregister.CashRegisterService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@RestController
@RequestMapping("/api/cashregister")
@CrossOrigin(origins = "http://localhost:4200")
public class CashRegisterController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CartController.class);

    private final CashRegisterService service;

    @Autowired
    public CashRegisterController(CashRegisterService service) {
        this.service = service;
    }


    @GetMapping
    public ResponseEntity getCashRegisters(@RequestParam(value = "id", required = false) String id,
                                           @RequestParam(value = "state", required = false) CashRegisterState state,
                                           @RequestParam(value = "daily", required = false) Date date,
                                           @RequestParam(value = "offset", required = false, defaultValue = "0") int page,
                                           @RequestParam(value = "limit", required = false, defaultValue = "5") int size,
                                           Sort sort
    ) {

        LOGGER.info("findAll: id: {} | state: {} | daily: {} | offset: {} | limit: {} | sort: {}", id, state, date, page, size, sort);

        PageRequest pageRequest = PageRequest.of(page, size, sort);

        if (id != null) {
            return new ResponseEntity(this.service.findById(id), HttpStatus.OK);
        }

        if (state != null) {
            return new ResponseEntity(this.service.findByState(state, pageRequest), HttpStatus.OK);
        }

        if (date != null) {
            return new ResponseEntity(this.service.findByDaily(date, pageRequest), HttpStatus.OK);
        }

        return new ResponseEntity(this.service.findAll(pageRequest), HttpStatus.OK);
    }



}
