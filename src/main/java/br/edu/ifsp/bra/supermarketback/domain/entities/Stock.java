package br.edu.ifsp.bra.supermarketback.domain.entities;

import br.edu.ifsp.bra.supermarketback.domain.models.Product.Product;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;
import java.util.Date;
import java.util.List;

@Document(collection = "Stock")
@Getter @Setter @NoArgsConstructor
public class Stock {

    @Id
    private String id;

    private List<Product> product;

    @NotEmpty
    private int quantity;

    private Date data;

}