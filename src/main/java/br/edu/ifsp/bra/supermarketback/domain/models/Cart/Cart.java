package br.edu.ifsp.bra.supermarketback.domain.models.Cart;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.redis.core.RedisHash;

import javax.persistence.Id;
import java.io.Serializable;
import java.util.List;

@RedisHash("Cart")
@Getter @Setter @EqualsAndHashCode
public class Cart implements Serializable {
    @Id
    private String id;
    private List<CartItem> items;
    private float total;


}
