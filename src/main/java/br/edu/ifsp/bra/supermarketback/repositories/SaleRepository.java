package br.edu.ifsp.bra.supermarketback.repositories;

import br.edu.ifsp.bra.supermarketback.domain.entities.Employee;
import br.edu.ifsp.bra.supermarketback.domain.entities.Sale;
import br.edu.ifsp.bra.supermarketback.domain.models.Cart.Cart;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

@RepositoryRestResource
@CrossOrigin(origins = "http://localhost:4200")
public interface SaleRepository extends PagingAndSortingRepository<Sale, String> {
    Page<Sale> findByEmployee(Employee employee, PageRequest pageRequest);
    Page<Sale> findByCart(Cart cart, PageRequest pageRequest);
}
