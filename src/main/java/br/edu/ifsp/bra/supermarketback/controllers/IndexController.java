package br.edu.ifsp.bra.supermarketback.controllers;

import br.edu.ifsp.bra.supermarketback.domain.entities.Employee;
import br.edu.ifsp.bra.supermarketback.domain.entities.Login;
import br.edu.ifsp.bra.supermarketback.services.employee.EmployeeService;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.Base64;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class IndexController {

    private EmployeeService service;

    public IndexController(EmployeeService service) {
        this.service = service;
    }

    @RequestMapping("/")
    @CrossOrigin(origins = "http://localhost:4200")
    String index() {
        return "index";
    }

    @PostMapping("/login")
    @CrossOrigin(origins = "http://localhost:4200")
    public Login login(@RequestBody Employee employee) {
        var permission =
                this.service.listAll().stream().anyMatch(
                        e -> e.getName().equals(employee.getName())
                        && e.getPassword().equals(employee.getPassword()));

        if (!permission) {
            return new Login(permission);
        }

        var user = this.service.listAll().stream().filter(e -> e.getName().equals(employee.getName())
                && e.getPassword().equals(employee.getPassword())).findFirst();


        return new Login(user.get(), permission);
    }

    @RequestMapping("/user")
    public Principal user(HttpServletRequest request) {
        String authToken = request.getHeader("Authorization")
                .substring("Basic".length()).trim();
        return () -> new String(Base64.getDecoder()
                .decode(authToken)).split(":")[0];
    }
}
