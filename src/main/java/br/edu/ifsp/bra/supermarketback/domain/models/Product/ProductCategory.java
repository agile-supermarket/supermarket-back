package br.edu.ifsp.bra.supermarketback.domain.models.Product;

import java.util.Arrays;

public enum ProductCategory {
    FOOD("FOOD"),
    DRINKS("DRINKS");

    private String value;

    ProductCategory(String value) {
        this.value = value;
    }

    public static ProductCategory fromType(String value) {
        for (var category: values()) {
            if (category.value.equalsIgnoreCase(value)) {
                return category;
            }
        }

        throw new IllegalArgumentException(
                "Unknown enum type " + value + ", Allowed values are "
                        + Arrays.toString(values())
        );
    }
}

