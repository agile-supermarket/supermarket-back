package br.edu.ifsp.bra.supermarketback.services.Stock;

import br.edu.ifsp.bra.supermarketback.domain.entities.Stock;
import br.edu.ifsp.bra.supermarketback.domain.models.Product.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.Optional;


public interface StockService {

    Page<Stock> listAll(PageRequest pageRequest);

    Optional<Stock> getById(String id);

    Stock save(Stock stock);

    void deleteById(String id);

}
