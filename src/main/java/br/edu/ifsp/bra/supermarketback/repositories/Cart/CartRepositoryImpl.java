package br.edu.ifsp.bra.supermarketback.repositories.Cart;

import br.edu.ifsp.bra.supermarketback.domain.models.Cart.Cart;
import br.edu.ifsp.bra.supermarketback.domain.models.Cart.CartItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Component
public class CartRepositoryImpl implements CartRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(CartRepositoryImpl.class);

    RedisTemplate<String, Cart> redisTemplate;

    @Autowired
    public CartRepositoryImpl(RedisTemplate<String, Cart> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }


    @Override
    public Cart getCartById(String id) {
        return this.redisTemplate.opsForValue().get(id);
    }

    @Override
    public Cart addToCart(String id, CartItem item) {
        float total = 0f;
        Cart cart = null;
        LOGGER.debug("id: {}", id);

        item.setId(UUID.randomUUID().toString());


        if ((id == null) || (id.equalsIgnoreCase(""))) {
            LOGGER.debug("missing id, creating new cart.");
            cart = createCart(UUID.randomUUID().toString(), item);
        } else {
            cart = getCartById(id);
            LOGGER.debug("Retrieve cart by existing id: {}, cart: {}", id, cart);

            if (cart == null) {
                cart = createCart(id, item);
            } else {
                cart.getItems().add(item);
            }
        }


        for (var cartItem : cart.getItems()) {
            cartItem.setPrice(cartItem.getProduct().getPrice() * cartItem.getQuantity());
            total += cartItem.getPrice();

        }

        cart.setTotal(total);
        LOGGER.debug("cart: " + cart);
        redisTemplate.opsForValue().set(cart.getId(), cart);

        return cart;
    }

    @Override
    public void removeToCart(String cardId, CartItem item) {
        Cart cart = null;
        if ((cardId == null) || (cardId.equalsIgnoreCase(""))) {
            LOGGER.debug("missing id.");
        } else {
            cart = getCartById(cardId);
            LOGGER.debug("Retrieve cart by existing id: {}, cart: {}", cardId, cart.toString());

            cart.getItems().remove(item);
            LOGGER.debug("cart: " + cart.toString());
            redisTemplate.opsForValue().set(cart.getId(), cart);

        }
    }

    private Cart createCart(String id, CartItem item) {
        List<CartItem> items = new ArrayList<CartItem>();
        items.add(item);
        var cart = new Cart();
        cart.setId(id);
        cart.setItems(items);
        cart.setTotal(item.getPrice());
        return cart;
    }
}
