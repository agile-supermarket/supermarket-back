package br.edu.ifsp.bra.supermarketback.services.cart;

import br.edu.ifsp.bra.supermarketback.domain.models.Cart.Cart;
import br.edu.ifsp.bra.supermarketback.domain.models.Cart.CartItem;

public interface CartService {

    Cart getCartById(String id);

    Cart addItemToCart(String id, CartItem item);

    void removeItemToCart(String id, CartItem item);
}
