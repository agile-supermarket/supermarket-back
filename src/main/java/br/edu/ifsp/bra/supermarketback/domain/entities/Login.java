package br.edu.ifsp.bra.supermarketback.domain.entities;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class Login {
    Employee employee;
    boolean permission;

    public Login(Employee employee, boolean permission) {
        this.employee = employee;
        this.permission = permission;
    }

    public Login(boolean permission) {
        this.employee = null;
        this.permission = permission;
    }
}
