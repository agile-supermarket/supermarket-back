package br.edu.ifsp.bra.supermarketback.domain.models.CashRegister;

import br.edu.ifsp.bra.supermarketback.domain.entities.Employee;
import br.edu.ifsp.bra.supermarketback.domain.entities.Sale;
import com.mongodb.lang.NonNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;
import java.util.Date;
import java.util.Set;


@Document(collection = "cash_register")
@Getter @Setter @NoArgsConstructor
public class CashRegister {
    @Id
    private String id;

    @NotEmpty @NonNull
    private Date daily;

    @NonNull
    private Employee employee;

    @NonNull
    private CashRegisterState state;

    private Set<Sale> sales;

    @NotEmpty
    private Float initCash;

    @NotEmpty
    private Float closedCash;

}
