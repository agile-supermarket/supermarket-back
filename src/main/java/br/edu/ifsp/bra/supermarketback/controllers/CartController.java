package br.edu.ifsp.bra.supermarketback.controllers;

import br.edu.ifsp.bra.supermarketback.domain.models.Cart.Cart;
import br.edu.ifsp.bra.supermarketback.domain.models.Cart.CartItem;
import br.edu.ifsp.bra.supermarketback.services.cart.CartService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/cart")
@CrossOrigin(origins = "http://localhost:4200")
public class CartController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CartController.class);

    private final CartService service;

    @Autowired
    public CartController(CartService service) {
        this.service = service;
    }

    @GetMapping(value = "/{id}")
    public Cart cart(@PathVariable("id") String id) {
        LOGGER.debug("Received request for cart by id: {}", id);
        var cart = service.getCartById(id);
        LOGGER.debug("Cart: {}", cart);
        return cart;
    }

    @PostMapping(value = "/{id}")
    public Cart cart(@PathVariable("id") String id, @RequestBody CartItem item) {
        LOGGER.debug("Received request to add item to cart by id: {}", id);
        var cart = service.addItemToCart(id, item);
        LOGGER.debug("Cart: {}", cart);
        return cart;
    }

    @PostMapping(value = "/")
    public Cart cart(@RequestBody CartItem item) {
        LOGGER.debug("Received request to add item to cart without id");
        var cart = service.addItemToCart(null, item);
        LOGGER.debug("Cart: {}", cart);
        return cart;
    }

    @PostMapping(value = "/remove/{id}")
    public ResponseEntity removeCart(@PathVariable("id") String id, @RequestBody CartItem item) {
        LOGGER.debug("Received request to remove item to cart by id: {}", id);
        service.removeItemToCart(id, item);
        return new ResponseEntity(HttpStatus.OK);
    }
}
