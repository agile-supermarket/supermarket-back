package br.edu.ifsp.bra.supermarketback.services.employee;

import br.edu.ifsp.bra.supermarketback.domain.entities.Employee;
import br.edu.ifsp.bra.supermarketback.repositories.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    private final EmployeeRepository employeeRepository;

    @Autowired
    public  EmployeeServiceImpl(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    @Override
    public List<Employee> listAll() {
        return this.employeeRepository.findAll();
    }

    @Override
    public List<Employee> listAdmins() {
        return this.employeeRepository.findAll().stream().filter(e -> e.isAdmin() == true).collect(Collectors.toList());
    }

    @Override
    public Optional<Employee> getById(String id) {
        return this.employeeRepository.findById(id);
    }

    @Override
    public Optional<Employee> getByName(String name) {
        return Optional.ofNullable(this.employeeRepository.findByName(name));
    }

    @Override
    public Employee insert(Employee employee) {
        return this.employeeRepository.insert(employee);
    }

    @Override
    public Employee update(Employee employee) {
        return this.employeeRepository.save(employee);
    }

    @Override
    public void delete(String id) {
        this.employeeRepository.deleteById(id);
    }
}
