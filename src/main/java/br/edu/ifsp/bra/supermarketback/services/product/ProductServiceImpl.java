package br.edu.ifsp.bra.supermarketback.services.product;

import br.edu.ifsp.bra.supermarketback.domain.models.Product.Product;
import br.edu.ifsp.bra.supermarketback.domain.models.Product.ProductCategory;
import br.edu.ifsp.bra.supermarketback.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ProductServiceImpl implements ProductService{

    private final ProductRepository repository;

    @Autowired
    public ProductServiceImpl(ProductRepository repository) {
        this.repository = repository;
    }


    @Override
    public Page<Product> listAll(PageRequest pageRequest) {
        return this.repository.findAll(pageRequest);
    }

    @Override
    public Optional<Product> getById(String id) {
        return this.repository.findById(id);
    }

    @Override
    public Page<Product> getByName(String name, PageRequest pageRequest) {
        return this.repository.findByName(name, pageRequest);
    }

    @Override
    public Page<Product> getByNameAndCategory(String name, ProductCategory category, PageRequest pageRequest) {
        return this.repository.findByNameAndCategory(name , category, pageRequest);
    }

    @Override
    public Page<Product> getByCategory(ProductCategory category, PageRequest pageRequest) {
        return this.repository.findByCategory(category, pageRequest);
    }

    @Override
    public Optional<Product> getByBarcode(String barcode) {
        return this.repository.findByBarcode(barcode);
    }

    @Override
    public Product save(Product product) {
        return repository.save(product);
    }

    @Override
    public void deleteById(String id) {
        repository.deleteById(id);
    }
}
