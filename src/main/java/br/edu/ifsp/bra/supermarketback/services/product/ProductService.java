package br.edu.ifsp.bra.supermarketback.services.product;

import br.edu.ifsp.bra.supermarketback.domain.models.Product.Product;
import br.edu.ifsp.bra.supermarketback.domain.models.Product.ProductCategory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.Optional;

public interface ProductService {
    Page<Product> listAll(PageRequest pageRequest);

    Optional<Product> getById(String id);

    Page<Product> getByName(String name, PageRequest pageRequest);

    Page<Product> getByNameAndCategory(String name, ProductCategory category, PageRequest pageRequest);

    Page<Product> getByCategory(ProductCategory category, PageRequest pageRequest);

    Optional<Product> getByBarcode(String barcode);

    Product save(Product employee);

    void deleteById(String id);
}
