package br.edu.ifsp.bra.supermarketback.messages;

import br.edu.ifsp.bra.supermarketback.domain.models.Product.Product;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ProductProducer {

    private final RabbitTemplate rabbitTemplate;

    @Autowired
    public ProductProducer(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    public void sendProductMessage(Product product) {
       rabbitTemplate.convertAndSend("br.edu.ifsp.bra.supermarket", product);
    }

}
