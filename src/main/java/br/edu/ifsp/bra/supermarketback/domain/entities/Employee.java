package br.edu.ifsp.bra.supermarketback.domain.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Document(collection = "employee")
@Getter @Setter @NoArgsConstructor
public class Employee {
    @Id
    private String id;

    @NotEmpty
    private String name;

    @NotEmpty
    private String email;

    @NotNull
    private String password;

    @NotNull
    private boolean admin;
}
