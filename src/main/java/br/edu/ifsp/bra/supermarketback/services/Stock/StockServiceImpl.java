package br.edu.ifsp.bra.supermarketback.services.Stock;

import br.edu.ifsp.bra.supermarketback.domain.entities.Stock;
import br.edu.ifsp.bra.supermarketback.repositories.StockRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
public class StockServiceImpl implements StockService{

    private StockRepository stockRepository;

    @Autowired
    public StockServiceImpl(StockRepository stockRepository) {
        this.stockRepository = stockRepository;
    }

    @Override
    public Page<Stock> listAll(PageRequest pageRequest) {
        return this.stockRepository.findAll(pageRequest);
    }

    @Override
    public Optional<Stock> getById(String id) {
        return this.stockRepository.findById(id);
    }

    @Override
    public Stock save(Stock stock) {
        return this.stockRepository.save(stock);
    }

    @Override
    public void deleteById(String id) {
        stockRepository.deleteById(id);
    }
}
