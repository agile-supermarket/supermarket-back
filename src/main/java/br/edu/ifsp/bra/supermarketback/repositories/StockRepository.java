package br.edu.ifsp.bra.supermarketback.repositories;

import br.edu.ifsp.bra.supermarketback.domain.entities.Stock;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.Optional;

@RepositoryRestResource
@CrossOrigin(origins = "http://localhost:4200")
public interface StockRepository extends PagingAndSortingRepository<Stock, String> {

    Optional<Stock> findById(String id);
}
