package br.edu.ifsp.bra.supermarketback.domain.entities;

import br.edu.ifsp.bra.supermarketback.domain.models.Cart.Cart;
import com.mongodb.lang.NonNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Id;
import java.io.Serializable;

@Document(collection = "sale")
@Getter @Setter @NoArgsConstructor
public class Sale implements Serializable {
 @Id
 private String id;

 @NonNull
 private Employee employee;

 @NonNull
 private Cart cart;

 @NonNull
 private Payments payments;

 @NonNull
 private float total;
}
