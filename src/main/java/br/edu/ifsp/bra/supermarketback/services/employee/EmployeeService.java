package br.edu.ifsp.bra.supermarketback.services.employee;

import br.edu.ifsp.bra.supermarketback.domain.entities.Employee;

import java.util.List;
import java.util.Optional;

public interface EmployeeService {
    List<Employee> listAll();

    List<Employee> listAdmins();

    Optional<Employee> getById(String id);

    Optional<Employee> getByName(String name);

    Employee insert(Employee employee);

    Employee update(Employee employee);

    void delete(String id);
}
