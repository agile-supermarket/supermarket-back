package br.edu.ifsp.bra.supermarketback.messages;

import br.edu.ifsp.bra.supermarketback.domain.models.Product.Product;
import br.edu.ifsp.bra.supermarketback.services.product.ProductService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class ProductConsumer {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProductConsumer.class);
    private final ProductService service;

    @Autowired
    public ProductConsumer(ProductService service) {
        this.service = service;
    }

    @RabbitListener(queues = "br.edu.ifsp.bra.supermarket")
    public void receiveMessage(Message message) {
        var messageBody = new String(message.getBody());

        LOGGER.info(messageBody);

        try {
            var mapper = new ObjectMapper();
            var product = mapper.readValue(message.getBody(), Product.class);
            var dbProduct = service.save(product);

            LOGGER.info("Product created: {}", dbProduct);
        } catch (IOException ex) {
           LOGGER.error("Erro ao converter a mensagem.", ex);
        }
    }

}
