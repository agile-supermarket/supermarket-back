package br.edu.ifsp.bra.supermarketback.services.sale;

import br.edu.ifsp.bra.supermarketback.domain.entities.Employee;
import br.edu.ifsp.bra.supermarketback.domain.entities.Sale;
import br.edu.ifsp.bra.supermarketback.domain.models.Cart.Cart;
import br.edu.ifsp.bra.supermarketback.repositories.SaleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class SaleServiceImpl implements SaleService {

    private final SaleRepository repository;

    @Autowired
    public SaleServiceImpl(SaleRepository repository) {
        this.repository = repository;
    }

    @Override
    public Sale save(Sale sale) {
        return this.repository.save(sale);
    }

    @Override
    public Optional<Sale> findById(String id) {
        return this.repository.findById(id);
    }

    @Override
    public Page<Sale> getAll(PageRequest pageRequest) {
        return this.repository.findAll(pageRequest);
    }

    @Override
    public Page<Sale> getByEmployee(Employee employee, PageRequest pageRequest) {
        return this.repository.findByEmployee(employee, pageRequest);
    }

    @Override
    public Page<Sale> getByCart(Cart cart, PageRequest pageRequest) {
        return this.repository.findByCart(cart, pageRequest);
    }
}
