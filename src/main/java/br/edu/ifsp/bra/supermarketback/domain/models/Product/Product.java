package br.edu.ifsp.bra.supermarketback.domain.models.Product;

import com.fasterxml.jackson.databind.annotation.JsonAppend;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.Date;

@Document(collection = "product")
@Getter @Setter @NoArgsConstructor
public class Product implements Serializable {
    @Id
    private String id;

    @NotEmpty
    private String name;

    @NotEmpty
    private String barcode;

    @NotEmpty
    private Float price;

    @NotEmpty
    private ProductCategory category;

    @NotEmpty
    private Date date_start;

    @NotEmpty
    private Date date_end;

    @NotEmpty
    private int ammount;


}
